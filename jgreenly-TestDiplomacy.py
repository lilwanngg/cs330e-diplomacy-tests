# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    def test_solve_1(self):
        r = StringIO(u"A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(u"A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n", w.getvalue())
    
    def test_solve_2(self):
        r = StringIO(u"A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(u"A Madrid\n", w.getvalue())
    
    def test_solve_3(self):
        r = StringIO(u"A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(u"A [dead]\nB Madrid\nC [dead]\nD Paris\n", w.getvalue())  

    def test_solve_4(self):
        r = StringIO(u"A Austin Move Houston\nB Houston Move Amarillo\nC Amarillo Support D\nD Lubbock Move Houston\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(u"A [dead]\nB [dead]\nC [dead]\nD [dead]\n", w.getvalue())


# ----
# main
# ----


if __name__ == "__main__": #pragma: no cover
    main()
