#!/usr/bin/env python3

# -------------------------------
# projects/Diplomacy/TestDiplomacy.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import *

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):


    def test_eval_1(self):
        v = diplomacy_eval('A Madrid Hold\n')
        self.assertEqual(v, 'A Madrid\n')

    def test_eval_2(self):
        v = diplomacy_eval('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n')
        self.assertEqual(v, 'A [dead]\nB Madrid\nC London\n')

    def test_eval_3(self):
        v = diplomacy_eval("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Barcelona\n")
        self.assertEqual(v, "A [dead]\nB [dead]\nC Barcelona\n")

    def test_eval_4(self):
        v = diplomacy_eval('A Austin Hold\nB Dallas Move Austin\nC Houston Move London\n')
        self.assertEqual(v, 'A [dead]\nB [dead]\nC London\n')

    def test_read(self):
    	s ="A Madrid Hold\n"
    	x = diplomacy_read(s)
    	self.assertEqual("A Madrid Hold\n", x) 	

    def test_solve_1(self):
    	r = StringIO("A London Hold\nB Dallas Move London\nC Houston Move SanAntonio")
    	w = StringIO()
    	diplomacy_solve(r,w)
    	self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC SanAntonio\n")

    def test_solve_2(self):
    	r = StringIO("A Madrid Hold\nB Barcelona Support A\nC Austin Support B\nD London Move Barcelona\nE Dallas Move Madrid\n")
    	w = StringIO()
    	diplomacy_solve(r,w)
    	self.assertEqual(w.getvalue(), "A [dead]\nB Barcelona\nC Austin\nD [dead]\nE [dead]\n")

    def test_solve_3(self):
    	r = StringIO("A Toronto Move London\nB Paris Move London\nC London Support A\n")
    	w = StringIO()
    	diplomacy_solve(r,w)
    	self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

	

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


% cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestDiplomacy.out



% cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
